%    This file "minimal-6-6-colored-notes.ly" is a LilyPond include file for
%    producing sheet music in Minimal 6-6 music notation, colored notes version.
%    version: 1.0.0
%    Copyright © 2023 Paul Morris
%
%    This file is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This file is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this file.  If not, see <http://www.gnu.org/licenses/>.

\version "2.20.0"

% Prefix with "m66-" to reduce the chance of name collisions.

#(define m66-cde-color "#5e50a1") % purple
#(define m66-fgab-color "#009c95") % turquoise

% #(define m66-cde-color (x11-color 'MidnightBlue))
% #(define m66-fgab-color (x11-color 'Black))

% #(define m66-cde-color "#00008b") % blue
% #(define m66-fgab-color "#8b0000") % red

#(define (m66-notehead-pitch grob)
   "Takes a note head grob and returns its pitch."
   (define event (ly:grob-property grob 'cause))
   (if (ly:stream-event? event)
       (ly:event-property event 'pitch)
       (begin
        (ly:warning "Cannot access the pitch of a note head grob.  (Are you trying to use the Ambitus_engraver?  It is not compatible.)")
        (ly:make-pitch 0 0 0))))

#(define (m66-notehead-semitone grob)
   "Takes a note head grob and returns its semitone."
   (ly:pitch-semitones (m66-notehead-pitch grob)))

#(define (m66-cde-note? grob)
   "Takes a note head grob object and returns a boolean indicating whether it
    is a note in the whole tone scale with C D E."
   (even? (m66-notehead-semitone grob)))

#(define (m66-get-note-head-color grob)
   "Takes a note head grob and returns a color string for that note."
   (if (m66-cde-note? grob)
       m66-cde-color
       m66-fgab-color))

%--- CONTEXT DEFINITIONS ----------------

% Copy standard settings of Staff and RhythmicStaff contexts
% to custom contexts named TradStaff and TradRhythmicStaff.
\layout {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  % Customize the Staff context.
  \context {
    \Staff
    \override NoteHead.color = #m66-get-note-head-color
    \override AmbitusNoteHead.color = #m66-get-note-head-color
    \override TrillPitchGroup.color = #m66-get-note-head-color
  }
}

% Let parent contexts accept TradStaff and TradRhythmicStaff in midi output.
\midi {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  \context {
    \Staff
    \name StaffClairnoteDN
    \alias Staff
  }
  \inherit-acceptability "StaffClairnoteDN" "Staff"
}
