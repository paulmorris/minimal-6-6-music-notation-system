%    This file "minimal-6-6-dotted-notes.ly" is a LilyPond include file for
%    producing sheet music in Minimal 6-6 music notation, dotted notes version.
%    version: 1.0.0
%    Copyright © 2023 Paul Morris
%
%    This file is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This file is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this file.  If not, see <http://www.gnu.org/licenses/>.

\version "2.20.0"

% Prefix with "m66-" to reduce the chance of name collisions.

#(define (m66-note-head-pitch grob)
   "Takes a note head grob and returns its pitch."
   (define event (ly:grob-property grob 'cause))
   (if (ly:stream-event? event)
       (ly:event-property event 'pitch)
       (begin
        (ly:warning "Cannot access the pitch of a note head grob.  (Are you trying to use the Ambitus_engraver?  It is not compatible.)")
        (ly:make-pitch 0 0 0))))

#(define (m66-stylish-note? grob)
   "Does a note head grob have one of various style properties."
   (define style (ly:grob-property-data grob 'style))
   ;; TODO: better handling of various note head styles
   ;; http://lilypond.org/doc/v2.24/Documentation/notation/note-head-styles
   ;; output-lib.scm
   (and (not (null? style))
        (memq style '(harmonic
                      harmonic-black
                      harmonic-mixed
                      diamond
                      cross
                      xcircle
                      triangle
                      slash))))

%{
#(define (center-stencil-on-stencil-axis axis stil-a stil-b)
   "Return a copy of stencil @var{stil-b} that has been
    moved so it is centered on stencil @var{stil-a} on
    @var{axis}. @var{axis} is 0 for X axis, 1 for Y axis."
   (ly:stencil-translate-axis
    (ly:stencil-aligned-to stil-b axis CENTER)
    (interval-center (ly:stencil-extent stil-a axis))
    axis))
%}

#(define (m66-center-stencil-on-stencil stil-a stil-b)
   "Return a copy of stencil @var{stil-b} that has been moved
    so it is centered on stencil @var{stil-a} on both X and Y axes."
   (ly:stencil-translate
    (centered-stencil stil-b)
    (cons
     (interval-center (ly:stencil-extent stil-a X))
     (interval-center (ly:stencil-extent stil-a Y)))))

#(define (m66-get-dot-note-head-stencil grob)
   (let*
    ((duration-log (ly:grob-property grob 'duration-log))
     (solid-head (> duration-log 1))
     (hollow-head (< duration-log 2))

     (dot-scale (cond (solid-head 1/3) (hollow-head 10/24)))
     (dot-color (cond (solid-head white) (hollow-head black)))

     (font (ly:grob-default-font grob))
     (solid-notehead-stencil (ly:font-get-glyph font "noteheads.s2"))

     (dot (stencil-with-color
           (flip-stencil X (ly:stencil-scale solid-notehead-stencil dot-scale dot-scale))
           dot-color))

     (stencil (ly:note-head::print grob))
     (centered-dot (m66-center-stencil-on-stencil stencil dot))

     (dotted-head (ly:stencil-add stencil centered-dot)))
    dotted-head))


#(define (m66-get-note-head-stencil grob)
   (let*
    ((fgab-note (odd? (ly:pitch-semitones (m66-note-head-pitch grob))))
     (stylish-note (m66-stylish-note? grob)))
    (cond
     (stylish-note (ly:note-head::print grob))
     (fgab-note (m66-get-dot-note-head-stencil grob))
     (else (ly:note-head::print grob)))))

%--- CONTEXT DEFINITIONS ----------------

% Copy standard settings of Staff and RhythmicStaff contexts
% to custom contexts named TradStaff and TradRhythmicStaff.
\layout {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  % Customize the Staff context.
  \context {
    \Staff
    \override NoteHead.stencil = #m66-get-note-head-stencil
  }
}

% Let parent contexts accept TradStaff and TradRhythmicStaff in midi output.
\midi {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  \context {
    \Staff
    \name StaffClairnoteDN
    \alias Staff
  }
  \inherit-acceptability "StaffClairnoteDN" "Staff"
}
