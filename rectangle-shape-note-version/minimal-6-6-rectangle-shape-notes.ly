%    This file "minimal-6-6-rectangle-shape-notes.ly" is a LilyPond include
%    file for producing sheet music in Minimal 6-6 music notation, rectangle
%    shape notes version.
%    version: 1.0.0
%    Copyright © 2023 Paul Morris
%
%    This file is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This file is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this file.  If not, see <http://www.gnu.org/licenses/>.

\version "2.20.0"

% Prefix with "m66-" to reduce the chance of name collisions.

#(define (m66-note-head-pitch grob)
   "Takes a note head grob and returns its pitch."
   (define event (ly:grob-property grob 'cause))
   (if (ly:stream-event? event)
       (ly:event-property event 'pitch)
       (begin
        (ly:warning "Cannot access the pitch of a note head grob.  (Are you trying to use the Ambitus_engraver?  It is not compatible.)")
        (ly:make-pitch 0 0 0))))

#(define (m66-stylish-note? grob)
   "Does a note head grob have one of various style properties."
   (define style (ly:grob-property-data grob 'style))
   ;; TODO: better handling of various note head styles
   ;; http://lilypond.org/doc/v2.24/Documentation/notation/note-head-styles
   ;; output-lib.scm
   (and (not (null? style))
        (memq style '(harmonic
                      harmonic-black
                      harmonic-mixed
                      diamond
                      cross
                      xcircle
                      triangle
                      slash))))

#(define (m66-get-note-head-stencil grob)
   "Takes a note head grob and returns a stencil.
    See other note head glyphs in the Emmentaler font used by LilyPond:
    https://lilypond.org/doc/v2.24/Documentation/notation/the-emmentaler-font "
   (let*
    ((pitch (m66-note-head-pitch grob ))
     (fgab-note (odd? (ly:pitch-semitones pitch)))
     ;; duration-log: 0 is whole, 1 is half, 2 is quarter and shorter.
     (duration-log (ly:grob-property grob 'duration-log))

     (quarter-head (> duration-log 1))
     (half-head (= duration-log 1))
     (whole-head (< duration-log 1))

     (font (ly:grob-default-font grob))
     (make-head (lambda (glyph-name x-scale y-scale)
                  (ly:stencil-scale (ly:font-get-glyph font glyph-name)
                                    x-scale y-scale))))
    (cond
     ((m66-stylish-note? grob) (ly:note-head::print grob))
     ;; FGAB (ovals)
     (fgab-note
      (cond
       (quarter-head (make-head "noteheads.s2sol" 1 1))
       (half-head (make-head "noteheads.s1sol" 1 1))
       (whole-head (make-head "noteheads.s0sol" 1 1))
       ))
     ;; CDE (rectangles)
     ((not fgab-note)
      (cond
       (quarter-head (make-head "noteheads.s2la" 1 1))
       (half-head (make-head "noteheads.s1la" 1 1))
       (whole-head (make-head "noteheads.s0la" 1 1))
       ))
     ;; Fallback: print default note head (should never happen).
     (else (ly:note-head::print grob))
     )))

#(define (m66-get-note-head-rotation grob)
   (let*
    ((pitch (m66-note-head-pitch grob ))
     (fgab-note (odd? (ly:pitch-semitones pitch)))
     ;; duration-log: 0 is whole, 1 is half, 2 is quarter and shorter.
     (duration-log (ly:grob-property grob 'duration-log))

     (quarter-head (> duration-log 1))
     (half-head (= duration-log 1))
     (whole-head (< duration-log 1))
     )
    (cond
     ((m66-stylish-note? grob) (ly:note-head::print grob))
     ;; FGAB (ovals)
     (fgab-note
      (cond
       (quarter-head '(-9 0 0))
       (half-head '(-9 0 0))
       (whole-head '(-9 0 0))
       ))
     ;; CDE (rectangles)
     ((not fgab-note)
      (cond
       (quarter-head '(0 0 0))
       (half-head '(0 0 0))
       (whole-head '(0 0 0))
       ))
     ;; Fallback (should never happen).
     (else '(0 0 0))
     )))

%--- CONTEXT DEFINITIONS ----------------

% Copy standard settings of Staff and RhythmicStaff contexts
% to custom contexts named TradStaff and TradRhythmicStaff.
\layout {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  % Customize the Staff context.
  \context {
    \Staff
    \override NoteHead.stencil = #m66-get-note-head-stencil
    % This allows experimenting with rotating the note heads.
    % \override NoteHead.rotation = #m66-get-note-head-rotation
  }
}

% Let parent contexts accept TradStaff and TradRhythmicStaff in midi output.
\midi {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  \context {
    \Staff
    \name StaffClairnoteDN
    \alias Staff
  }
  \inherit-acceptability "StaffClairnoteDN" "Staff"
}
