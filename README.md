# Minimal 6-6 Music Notation LilyPond Files

LilyPond files (.ly) for creating sheet music in one of several versions of
[Minimal 6-6 Music Notation System](https://musicnotation.org/wiki/notation-systems/minimal-6-6-notation-system-by-paul-morris/).
Each version has its own directory.

To use the files simply
[`\include`](https://lilypond.org/doc/v2.24/Documentation/notation/including-lilypond-files)
them at the top of a LilyPond sheet music file (.ly).
Then when you run LilyPond on the file the sheet music will be in Minimal 6-6
Music Notation.
