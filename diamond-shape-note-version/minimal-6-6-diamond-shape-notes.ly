%    This file "minimal-6-6-diamond-shape-notes.ly" is a LilyPond include
%    file for producing sheet music in Minimal 6-6 music notation, diamond
%    shape notes version.
%    version: 1.0.0
%    Copyright © 2023 Paul Morris
%
%    This file is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This file is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this file.  If not, see <http://www.gnu.org/licenses/>.

\version "2.20.0"

% Prefix with "m66-" to reduce the chance of name collisions.

#(define (m66-note-head-pitch grob)
   "Takes a note head grob and returns its pitch."
   (define event (ly:grob-property grob 'cause))
   (if (ly:stream-event? event)
       (ly:event-property event 'pitch)
       (begin
        (ly:warning "Cannot access the pitch of a note head grob.  (Are you trying to use the Ambitus_engraver?  It is not compatible.)")
        (ly:make-pitch 0 0 0))))

#(define (m66-notehead-style grob)
   "Takes a note head grob and returns a NoteHead.style property value."
   (define pitch (m66-note-head-pitch grob))
   (if (even? (ly:pitch-semitones pitch))
       'solFunk
       'miFunk
       ;; Some other possible values:
       ;; 'default
       ;; 'mi
       ;; 'fa
       ;; 'diamond
       ;; 'laFunk
       ;; 'la
       ;; 'harmonic
       ))

%--- CONTEXT DEFINITIONS ----------------

% Copy standard settings of Staff and RhythmicStaff contexts
% to custom contexts named TradStaff and TradRhythmicStaff.
\layout {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  % Customize the Staff context.
  \context {
    \Staff
    \override NoteHead.style = #m66-notehead-style
  }
}

% Let parent contexts accept TradStaff and TradRhythmicStaff in midi output.
\midi {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  \context {
    \Staff
    \name StaffClairnoteDN
    \alias Staff
  }
  \inherit-acceptability "StaffClairnoteDN" "Staff"
}
