%    This file "minimal-6-6-flipped-notes.ly" is a LilyPond include file for
%    producing sheet music in Minimal 6-6 music notation, flipped notes version.
%    version: 1.0.0
%    Copyright © 2023 Paul Morris
%
%    This file is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This file is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this file.  If not, see <http://www.gnu.org/licenses/>.

\version "2.20.0"

% Prefix with "m66-" to reduce the chance of name collisions.

#(define (m66-note-head-pitch grob)
   "Takes a note head grob and returns its pitch."
   (define event (ly:grob-property grob 'cause))
   (if (ly:stream-event? event)
       (ly:event-property event 'pitch)
       (begin
        (ly:warning "Cannot access the pitch of a note head grob.  (Are you trying to use the Ambitus_engraver?  It is not compatible.)")
        (ly:make-pitch 0 0 0))))

#(define (m66-stylish-note? grob)
   "Does a note head grob have one of various style properties."
   (define style (ly:grob-property-data grob 'style))
   ;; TODO: better handling of various note head styles
   ;; http://lilypond.org/doc/v2.24/Documentation/notation/note-head-styles
   ;; output-lib.scm
   (and (not (null? style))
        (memq style '(harmonic
                      harmonic-black
                      harmonic-mixed
                      diamond
                      cross
                      xcircle
                      triangle
                      slash))))

#(define (m66-cde-note? grob)
   (even? (ly:pitch-semitones (m66-note-head-pitch grob))))

#(define (m66-get-note-head-stencil grob)
   (let*
    ((cde-note (m66-cde-note? grob))
     (stylish-note (m66-stylish-note? grob))
     (default-stencil (ly:note-head::print grob)))
    (cond
     (stylish-note default-stencil)
     (cde-note (flip-stencil X default-stencil))
     (else default-stencil))))

#(define (m66-get-stem-attachment grob)
   (let*
    ((cde-note (m66-cde-note? grob))
     (stylish-note (m66-stylish-note? grob))
     (default-stem-attachment (ly:note-head::calc-stem-attachment grob)))
    (cond
     (stylish-note default-stem-attachment)
     (cde-note (cons (car default-stem-attachment)
                     (- (cdr default-stem-attachment))))
     (else default-stem-attachment))))

%--- CONTEXT DEFINITIONS ----------------

% Copy standard settings of Staff and RhythmicStaff contexts
% to custom contexts named TradStaff and TradRhythmicStaff.
\layout {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  % Customize the Staff context.
  \context {
    \Staff
    \override NoteHead.stencil = #m66-get-note-head-stencil
    \override NoteHead.stem-attachment = #m66-get-stem-attachment
  }
}

% Let parent contexts accept TradStaff and TradRhythmicStaff in midi output.
\midi {
  \context {
    \Staff
    \name TradStaff
    \alias Staff
  }
  \inherit-acceptability "TradStaff" "Staff"

  \context {
    \RhythmicStaff
    \name TradRhythmicStaff
    \alias RhythmicStaff
  }
  \inherit-acceptability "TradRhythmicStaff" "RhythmicStaff"

  \context {
    \Staff
    \name StaffClairnoteDN
    \alias Staff
  }
  \inherit-acceptability "StaffClairnoteDN" "Staff"
}
